﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Final_Test
{
    public partial class FAT : Form
    {

        public FAT()
        {
            InitializeComponent();
            setConnection();
            loadData();
        }
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter dataAdapter;
        private DataSet dataSet;
        private BindingSource bindingSource;
        private SqlConnection setConnection()
        {
            string str = "server=.;database=BookDB;uid=sa;pwd=123";
            connection = new SqlConnection(str);
            return connection;
        }
        private void loadData()
        {
            try 
            {
                string getCate = "select * from Books";// WHERE [BookTitle] LIKE '%@title%'";
                //command = new SqlCommand(getCate, connection);
                //command.Parameters.AddWithValue("@title", txtFilter.Text);
                //dataAdapter = new SqlDataAdapter(command);
                dataAdapter = new SqlDataAdapter(getCate, connection);
                dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Books");
                bindingSource = new BindingSource();
                bindingSource.DataSource = dataSet.Tables["Books"];
                dataGridView1.DataSource = bindingSource;
                dataGridView1.Sort(dataGridView1.Columns[3], ListSortDirection.Descending);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Regex.IsMatch(txtISBN.Text, "^[1-9]{1,}$"))
                {
                    MessageBox.Show("ISBN requied....");
                    txtISBN.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtTitle.Text))
                {
                    MessageBox.Show("Title requied....");
                    txtFilter.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtAuthor.Text))
                {
                    MessageBox.Show("Author requied....");
                    txtAuthor.Focus();
                    return;
                }
                else if (!Regex.IsMatch(txtEdition.Text, "^[1-9]{1,}$"))
                {
                    MessageBox.Show("Edition requied....");
                    txtEdition.Focus();
                    return;
                }
                string sql = "insert into Books values (@ISBN,@Title,@Author,@Edititon)";
                command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@ISBN", int.Parse(txtISBN.Text));
                command.Parameters.AddWithValue("@Title", txtTitle.Text);
                command.Parameters.AddWithValue("@Author", txtAuthor.Text);
                command.Parameters.AddWithValue("@Edititon", int.Parse(txtEdition.Text));
                connection.Open();
                command.ExecuteNonQuery();
                MessageBox.Show("Create New Book Successfully");
                loadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            bindingSource.MoveLast();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            bindingSource.MoveFirst();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                int row = dataGridView1.CurrentRow.Index;       //chọn dòng hiện hành
                int isbn = int.Parse(dataGridView1.Rows[row].Cells[0].Value.ToString());
                string title = dataGridView1.Rows[row].Cells[1].Value.ToString();
                string author = dataGridView1.Rows[row].Cells[2].Value.ToString();
                int edition = int.Parse(dataGridView1.Rows[row].Cells[3].Value.ToString());

                string query = "UPDATE Books SET [BookTitle]=@title,Author=@author,Edition=@edition Where ISBN=@isbn";
                command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@title", title);
                command.Parameters.AddWithValue("@author", author);
                command.Parameters.AddWithValue("@edition", edition);
                command.Parameters.AddWithValue("@isbn", isbn);
                connection.Open();
                command.ExecuteNonQuery();
                loadData();
                MessageBox.Show("Update Book Successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            loadData();
        }
    }
}
